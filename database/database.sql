CREATE DATABASE IF NOT EXISTS proyectoFinal;
    use proyectoFinal;

    create table  usuario (
        id int(11) primary key auto_increment,
        nombre varchar(60) not null,
        email varchar(60) not null, 
        edad int(11) not null
    );
